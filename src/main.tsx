import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import './index.css'
import {
  createBrowserRouter,
  RouterProvider,
} from 'react-router-dom';
import RessourcePage from './pages/RessourcePage.tsx';
import ConnexionPage from './pages/ConnexionPage.jsx';
//import TestPage from './pages/TestPage.tsx'
import { startReactDsfr } from "@codegouvfr/react-dsfr/spa";
import RessourceInfo from './pages/RessourceInfoPage.tsx';
startReactDsfr({ defaultColorScheme: "system" });



const router = createBrowserRouter([
  { path: '/', element: <App /> },
  { path: '/connexion', element: <ConnexionPage />},
  { path: '/ressources', element: <RessourcePage /> },
  { path: '/carte', element: <RessourcePage /> },
  { path: '/ressource/:id', element: <RessourceInfo /> },
]);

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)
