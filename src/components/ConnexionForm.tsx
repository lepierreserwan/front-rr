import { useEffect, useState } from "react";
import { useCookies } from "react-cookie";
import { User } from "../model/User";
import { fr } from "@codegouvfr/react-dsfr";
import userData from '../features/user.json';
import { useIsDark } from "@codegouvfr/react-dsfr/useIsDark";

export default function ConnexionForm() {
    
    const { isDark, setIsDark } = useIsDark();
    const [password, setPassword] = useState(false);
    const [cookie, setCookie,removeCookie] = useCookies(['user'])
    async function search(user){
        console.log(user)
        for (let i = 0; i < userData.length; i++){
            if ( userData[i].mail == user.mail && userData[i].password == user.password){
                alert('Votre nom existe !!')
                const userdata = userData[i]
                setCookie('user', userdata, {path:'/'})
            }
            else{
                alert('popopop non')
            }
        }
    }

    const [user, setUser] = useState<User>({
        id: '',
        nom: '',
        prenom: '',
        mail: '',
        ville: '',
        age: 0,
        password: '',
        roleID: ''
    });

    return (
        <main className={fr.cx("")} role="main" id="content">
        <button className="p-4 bg-blue-300" onClick={(e)=>{e.preventDefault(); removeCookie('user', {path:'/'})}}>Remove cookie</button>
            <div className={fr.cx("fr-container fr-container--fluid fr--md-14v fr-responsive-container")}>
                <div className={fr.cx("fr-grid-row fr-grid-row-gutters fr-grid-row--center")}>
                    <div className={fr.cx("fr-col-12 fr-col-md-8 fr-col-lg-8")}>
                        <div className={isDark ? "bg-slate-800" : "bg-slate-100"}>
                            <div className={fr.cx("fr-container fr-background-alt--grey fr-px-md-0 fr-py-10v fr-py-md-14v")}>
                                <div className={fr.cx("fr-grid-row fr-grid-row-gutters fr-grid-row--center")}>
                                    <div className={fr.cx("fr-col-12 fr-col-md-9 fr-col-lg-10")}>
                                        <h1 className={fr.cx("fr-h1")}>Connexion à Ressource Relationnel</h1>
                                        <div className={fr.cx("fr-mb-6v")}>
                                            <h2 className={fr.cx("fr-h2")}>Se connecter avec FranceConnect</h2>
                                            <div className={fr.cx("fr-connect-group")}>
                                                <a href="https://franceconnect.gouv.fr/">
                                                    <button className={fr.cx("fr-connect")}>
                                                        <span className={fr.cx("fr-connect__login")}>S’identifier avec</span>
                                                        <span className={fr.cx("fr-connect__brand")}>FranceConnect</span>

                                                    </button>
                                                </a>
                                                <p>
                                                    <a href="https://franceconnect.gouv.fr/" target="_blank" rel="noopener" title="Qu’est ce que FranceConnect ? - nouvelle fenêtre">Qu’est ce que FranceConnect ?</a>
                                                </p>
                                            </div>
                                        </div>
                                        <p className={fr.cx("fr-hr-or")}>ou</p>
                                        <div>
                                            <form onSubmit={(e)=>{e.preventDefault();search(user)}}>
                                                <fieldset className={fr.cx("fr-fieldset")} aria-labelledby="login-1760-fieldset-legend login-1760-fieldset-messages">
                                                    <legend className={fr.cx("fr-fieldset__legend")}>
                                                        <h2 className={fr.cx("fr-h2")}>Se connecter avec son compte</h2>
                                                    </legend>
                                                    <div className={fr.cx("fr-fieldset__element")}>
                                                        <fieldset className={fr.cx("fr-fieldset")} aria-labelledby="credentials-messages">
                                                            <div className={fr.cx("fr-fieldset__element")}>
                                                                <span className={fr.cx("fr-hint-text")}>Sauf mention contraire, tous les champs sont obligatoires.</span>
                                                            </div>
                                                            <div className={fr.cx("fr-fieldset__element")}>
                                                                <div className={fr.cx("fr-input-group")}>
                                                                    <label className={fr.cx("fr-label")}>
                                                                        Identifiant
                                                                        <span className={fr.cx("fr-hint-text")}>Format attendu : nom@domaine.fr</span>
                                                                    </label>
                                                                    <input className={fr.cx("fr-input")} aria-required="true"
                                                                        onInput={(e) => setUser({ ...user, mail: e.target.value })} required aria-describedby="username-1757-messages" name="username" type="email" />
                                                                    <div className={fr.cx("fr-messages-group")} aria-live="assertive">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className={fr.cx("fr-fieldset__element")}>
                                                                <div className={fr.cx("fr-password")}>
                                                                    <label className={fr.cx("fr-label")}>
                                                                        Mot de passe
                                                                    </label>
                                                                    <div className={fr.cx("fr-input-wrap")}>
                                                                        <input className={fr.cx("fr-password__input fr-input")} onInput={(e)=>{setUser({...user, password: e.target.value})}} required title="Le mot de passe est requis" name="password" type={password ? "text" : "password"} />
                                                                    </div>
                                                                    <div className={fr.cx("fr-messages-group")} aria-live="assertive">
                                                                    </div>
                                                                    <div className={fr.cx("fr-password__checkbox fr-checkbox-group fr-checkbox-group--sm")}>
                                                                        <input type="checkbox" aria-checked={password} checked={password} onChange={() => setPassword(!password)} className="z-10" />
                                                                        <label className={fr.cx("fr-password__checkbox fr-label")}>
                                                                            Afficher
                                                                        </label>
                                                                        <div className={fr.cx("fr-messages-group")} aria-live="assertive">
                                                                        </div>
                                                                    </div>
                                                                    <p>
                                                                        <a href="[À MODIFIER - url de la page de récupération]" className={fr.cx("fr-link")}>Mot de passe oublié ?</a>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div className={fr.cx("fr-messages-group")} aria-live="assertive">
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                    <div className={fr.cx("fr-fieldset__element")}>
                                                        <div className={fr.cx("fr-checkbox-group fr-checkbox-group--sm")}>
                                                            <input name="remember" className="z-10" type="checkbox" aria-describedby="remember-1759-messages" />
                                                            <label className={fr.cx("fr-label")}>
                                                                Se souvenir de moi
                                                            </label>
                                                            <div className={fr.cx("fr-messages-group")} aria-live="assertive">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className={fr.cx("fr-fieldset__element")}>
                                                        <ul className={fr.cx("fr-btns-group")}>
                                                            <li>
                                                                <button className={fr.cx("fr-mt-2v fr-btn")} type="submit">
                                                                    Se connecter
                                                                </button>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div className={fr.cx("fr-messages-group")} aria-live="assertive">
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>
                                        <h2>Vous n’avez pas de compte ?</h2>
                                        <ul className={fr.cx("fr-btns-group")}>
                                            <li>
                                                <button className={fr.cx("fr-btn fr-btn--secondary")}>
                                                    Créer un compte
                                                </button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

    )
}