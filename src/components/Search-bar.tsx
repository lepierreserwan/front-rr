import Autocomplete from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';
import { City } from '../model/City'; // Assurez-vous d'avoir défini le type City
import { FilterOptionsState } from '@mui/material';

type MySearchInputProps = {
  cities: City[];
  onCitySelect: (city: City | null) => void;
  onMapCenterChange: (center: [number, number]) => void; // Nouvelle prop pour mettre à jour le centre de la carte
};

function MySearchInput(props: MySearchInputProps) {
  const { cities, onCitySelect, onMapCenterChange } = props;

  const filterOptions = (
    options: City[],
    { inputValue }: FilterOptionsState<City>
  ) => {
    return inputValue.length >= 3
      ? options.filter((city) =>
          city.label.toLowerCase().includes(inputValue.toLowerCase())
        )
      : [];
  };

  const handleCitySelect = (city: City | null) => {
    onCitySelect(city); // Appel de la fonction onCitySelect avec la ville sélectionnée
    if (city && cities.includes(city)) {
      // Mise à jour du centre de la carte si une ville est sélectionnée
      onMapCenterChange([parseFloat(city.latitude), parseFloat(city.longitude)]);
    }
  };

  return (
    <div>
    <Autocomplete
      options={cities}
      getOptionLabel={(city) => city.label + ' ' + city.zip_code}
      onChange={(event, newValue) => handleCitySelect(newValue)} // Utilisation de la nouvelle fonction de gestion de la sélection
      renderInput={(params) => (
        <TextField
          {...params}
          label="Rechercher une ville"
          variant="outlined"
          style={{ width: '300px', color:'white' }}
        />
      )}
      filterOptions={filterOptions}
    />
    </div>
  );
}

export default MySearchInput;