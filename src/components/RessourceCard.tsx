import moment from "moment";
import { Card } from "@codegouvfr/react-dsfr/Card";
import { Badge } from "@codegouvfr/react-dsfr/Badge";
import { Highlight } from "@codegouvfr/react-dsfr/Highlight";
import { Button } from "@codegouvfr/react-dsfr/Button";
import { Link } from "react-router-dom";

export default function RessourceCard(props){
   
    const id = props.ressourceInfo.id;
    const title =props.ressourceInfo.title;
    const date = moment(props.ressourceInfo.eventDate).format('DD-MM-YYYY HH:mm');
    const address = props.ressourceInfo.address;
    const desc = props.ressourceInfo.description;
    //const route = props.ressourceInfo['@id'].substring(1)
    const detail = (
        <div>
            <p>Date: {date}</p>
            <p>Lieu: {address}</p>
            <Highlight>
                <p>{desc}</p>
            </Highlight>
        </div>
    );
    

    return  <div className="card" style={{backgroundColor:'gray',display:"flex", flexDirection:"column", width:'80%', marginBottom:'10px', borderRadius:'5px'}}>
         <Link to={`/ressource/${id}`} style={{ textDecoration: 'none', color: 'inherit' }}>
               <Card
                background
                badge={<div><Badge>LABEL BADGE</Badge><Badge>LABEL BADGE</Badge></div>}
                border
                desc={detail}
                horizontal
                imageAlt="texte alternatif de l’image"
                imageUrl="https://www.systeme-de-design.gouv.fr/img/placeholder.16x9.png"
                linkProps={{
                href: 'ressource'
                }}
                size="small"
                title={title}
                footer ={<ul className="fr-btns-group fr-btns-group--inline-reverse fr-btns-group--inline-lg"><li><Button
                        iconId="fr-icon-checkbox-circle-line"
                        onClick={function noRefCheck(){}}
                        priority="tertiary no outline"
                        title="Label button"
                    /></li><li><button className="fr-btn">Label</button></li></ul>}
                shadow
                titleAs="h3"
                />  
                </Link>
            </div>

               
}
