import { MainNavigation } from "@codegouvfr/react-dsfr/MainNavigation";
import { Header } from "@codegouvfr/react-dsfr/Header";
import { useCookies } from "react-cookie";
import { headerFooterDisplayItem } from "@codegouvfr/react-dsfr/Display";
import { useEffect, useState } from "react";
export default function NavBar() {
  const [cookie, setCookie] = useCookies(['user'])
  const [linkprop, setLinkprop] = useState({
    href: '/connexion', target: '_self'
  })
  const [text_prop, setText_prop] = useState('Se connecter')
  // console.log(cookie.user)
  useEffect(() => {
    switch (cookie.user) {
      case undefined:
        setLinkprop({ href: '/connexion', target: '_self' })
        setText_prop('Se connecter')
        break;
      default:
        setLinkprop({ href: '/compte', target: '_self' })
        setText_prop('Mon compte')
      break;
    }
  }, [cookie.user])
  return <div style={{ flex: 1 }}>
    <Header style={{ maxWidth: '' }}
      quickAccessItems={[

        // other quick access items...
        headerFooterDisplayItem,
        {
          buttonProps: {
            onClick: function noRefCheck() { }
          },
          iconId: 'ri-account-box-line',
          text: text_prop,
          linkProps: 
            linkprop
          
        },

      ]}

      brandTop={<>RESSOURCE<br />RELATIONNELLE</>}
      homeLinkProps={{
        href: '/',
        title: 'Accueil - Nom de l’entité (ministère, secrétariat d‘état, gouvernement)'
      }}
      id="fr-header-navigation-as-custom-node"
      navigation={<MainNavigation items={[
        { linkProps: { href: '/', target: '_self' }, text: 'Accueil' },
        { linkProps: { href: '/carte', target: '_self' }, text: 'Carte' },
        { linkProps: { href: '/ressources', target: '_self' }, text: 'Ressources' },
      ]} />}
    />

  </div>
}