/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */
import NavBar from './components/NavBar';
import FooterComp from './components/Footer';
import { MapContainer } from 'react-leaflet/MapContainer'
import { TileLayer } from 'react-leaflet/TileLayer'
import { Marker, Popup, useMap, useMapEvents } from 'react-leaflet';
import { Ressource } from './model/Ressource';
import { useEffect, useRef, useState } from 'react';
import moment from 'moment';
import RessourceCardSmall from './components/RessourceCardSmall';
import { City } from './model/City';
import MySearchInput from './components/Search-bar';
import { Link } from 'react-router-dom';
import ArrowCircleRightOutlinedIcon from '@mui/icons-material/ArrowCircleRightOutlined';
import { IconButton } from '@mui/material';
//import cities from './data/cities.json';

const BASE_URL = 'http://localhost:8000/api';

function useChangeMapCenter() {
  const map = useMap();

  return (center: any[]) => {
    console.log('center', center);
    const lat = {lat : center[0], lng : center[1]}
    console.log('lat', lat)
    map.flyTo(lat, map.getZoom());
  };
}

function App() {
    const [mapInstance, setMapInstance] = useState(null);
    const [error, setError] = useState();
    const [isLoading, setIsLoading] = useState(false);
    const [donnees, setDonnees] = useState<Ressource[]>([]);
    const [filter, setFilter] = useState(moment().format('YYYY-MM-DDTHH:mm:ss'));
    const [cities, setCities] = useState<City[]>([]);
    const [selectedCity, setSelectedCity] = useState<City | null>(null);
    const [mapCenter, setMapCenter] = useState<[number, number]>([44.80, -0.40]);

    const mapRef = useRef<any>(null);

    function MapInstanceProvider({ setMapInstance }) {
      const map = useMap();
    
      useEffect(() => {
        setMapInstance(map);
      }, [map, setMapInstance]);
    
      return null;
    }

    useEffect(() => {
		const fetchData = async () => {
			setIsLoading(true);
            moment.locale('fr');
			try { 
				const response = await fetch(`${BASE_URL}/ressources?eventDate[after]=${filter}`);

				if (response.ok) {
					const responseData = await response.json();
                    const members = responseData['hydra:member'];           
            setDonnees(members);
					setIsLoading(false);
          
				} else {
				console.error('Erreur lors de la récupération des données');
				}

			} catch (e: any) {
				console.error('Erreur inattendue: ', e);
				setError(e);
			} finally {
				setIsLoading(false);
			}
		};
		fetchData();

        const fetchCities = async () => {
            try {
              // Charger le fichier JSON
              const response = await fetch('https://www.data.gouv.fr/fr/datasets/r/521fe6f9-0f7f-4684-bb3f-7d3d88c581bb');
              const data = await response.json();
                console.log(data.cities);
                const uniqueCitiesData = data.cities.filter((city:any, index:any, self:any) =>
                    index === self.findIndex((t:any) => t.label === city.label)
                  );

            const citiesData = uniqueCitiesData.map((item:any, index:any) => {
            return { ...item, id: index + 1 }; // Ajouter un identifiant auto-incrémenté
            });
            setCities(citiesData);
            } catch (error) {
              console.error('Erreur lors du chargement des villes : ', error);
            }
          };
      
        fetchCities();

        if (mapCenter && mapRef.current) {
            mapRef.current.setView(mapCenter, 13);
          }
        }, [mapCenter]);
      
        const handleMapCenterChange = (center: any[]) => {
          console.log('center', center);
          const lat = {lat : center[0], lng : center[1]}
          console.log('lat', lat)
          if (mapInstance) {
            mapInstance.flyTo(lat, mapInstance.getZoom());
          }
        };

        function LocationMarker() {
          const [position, setPosition] = useState(null)
          const map = useMapEvents({
            click() {
              map.locate()
            },
            locationfound(e) {
              console.log(e)
              setPosition(e.latlng)
              map.flyTo(e.latlng, map.getZoom())
            },
          })
        
          return position === null ? null : (
            <Marker position={position}>
              <Popup>You are here</Popup>
            </Marker>
          )
        }

    return (
    <div>
      <NavBar />
      <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', height: '80vh' }}>
        <div className='Title'><h2>Ressource Relationnelle</h2></div>
        <MySearchInput cities={cities} onCitySelect={setSelectedCity} onMapCenterChange={handleMapCenterChange} />
        <MapContainer center={mapCenter} zoom={13} scrollWheelZoom={true} style={{ height: '500px', width: '90%', display: 'flex', justifyContent: 'center', alignItems: 'center', marginBottom: '40px', marginTop: '40px' }}>
        <MapInstanceProvider setMapInstance={setMapInstance} />
          <TileLayer
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          {donnees && donnees.map((ressource: Ressource, index: number) => (
            <Marker key={index} position={[ressource.latitude, ressource.longitude]}>
               <Link to={`/ressource/${ressource.id}`} style={{ textDecoration: 'none', color: 'inherit' }}>
                <Popup>
                  {ressource.title}<br></br>
                  {moment(ressource.eventDate).format('DD-MM-YYYY HH:mm')}
                  <a style={{display:'flex', justifyContent:'flex-end'}} href={`/ressource/${ressource.id}`}><IconButton aria-label="delete">
                    <ArrowCircleRightOutlinedIcon />
                  </IconButton></a>
                </Popup>
              </Link>
            </Marker>
          ))}
          <LocationMarker />
        </MapContainer>
      </div>
      
      <FooterComp />
    </div>
  );
}


export default App;
