import {Breadcrumb} from '@codegouvfr/react-dsfr/Breadcrumb'
import NavBar from '../components/NavBar'
import FooterComp from '../components/Footer'
import ConnexionForm from '../components/ConnexionForm'

export default function ConnexionPage(){
    return(
        <div className="h-full">
        <NavBar /> 
           <Breadcrumb
        segments={[
          { label: 'Accueil', linkProps: { href: '/' } }
        ]}
        className="ml-8"
        currentPageLabel="Connexion"
      />
      <div className="mb-8">
        <ConnexionForm />
      </div>
      <FooterComp />
        </div>
    )
}