import { Link, useParams } from 'react-router-dom';
import { useState, useEffect} from 'react';
import { Ressource } from '../model/Ressource';
import NavBar from '../components/NavBar';
import FooterComp from '../components/Footer';
import moment from 'moment';
import { MapContainer } from 'react-leaflet/MapContainer'
import { TileLayer } from 'react-leaflet/TileLayer'
import { Marker, Popup } from 'react-leaflet';
import { Button } from "@codegouvfr/react-dsfr/Button";
import Countdown from 'react-countdown';
import { IconButton } from '@mui/material';
import ArrowCircleLeftOutlinedIcon from '@mui/icons-material/ArrowCircleLeftOutlined';

const BASE_URL = 'http://localhost:8000/api';
export default function RessourceInfo() {
    const { id } = useParams();
    const [error, setError] = useState();
    const [isLoading, setIsLoading] = useState(false);
    const [donnees, setDonnees] = useState<Ressource>();
    const [windowWidth, setWindowWidth] = useState(window.innerWidth);
    let delay = moment(donnees?.eventDate).diff(moment(), 'seconds');
    delay = Date.now() + (delay * 1000);

    useEffect(() => {
		const fetchData = async () => {
			setIsLoading(true);
			try { 
				const response = await fetch(`${BASE_URL}/ressources/${id}`);
				if (response.ok) {
					const responseData = await response.json();
                    setDonnees(responseData);
					setIsLoading(false);
				} else {
				console.error('Erreur lors de la récupération de la ressource');
				}
			} catch (e: any) {
				console.error('Erreur inattendue: ', e);
				setError(e);
			} finally {
				setIsLoading(false);
			}
		};
		fetchData();

        const handleResize = () => {
            setWindowWidth(window.innerWidth);
          };
      
          window.addEventListener('resize', handleResize);
      
          return () => {
            window.removeEventListener('resize', handleResize);
          };
	}, []);

 


    return (
            <div>
                <NavBar/> 
                <Link to={`/ressources`} style={{ textDecoration: 'none', color: 'inherit' }}>
                    <div style={{display:"flex", flexDirection:"column", marginBottom:'10px',marginTop:'10px', width:'120px'}}>
                        <IconButton style={{display:'flex', justifyContent:'flex-end'}} href={`/ressource/${id}`} aria-label="return">
                    <ArrowCircleLeftOutlinedIcon /> Retour
                  </IconButton>
                    </div>
                </Link>
                <div style={{display:'flex', alignItems:'center', width:'100%', flexDirection:'column'}}>
                    <div style={{display:"flex", alignItems:'center', flexDirection:"column", width:'90%', marginBottom:'10px'}}>
                        <h1>{donnees?.title}</h1>
                        <div style={{backgroundColor:'red',display:'flex', height:'2px', width:'90%', justifyContent:'center', marginBottom:'20px'}}></div>
                            <p>{donnees?.description}</p>
                            
                            <div style={{display:"flex", flexDirection:"row", width:'90%', marginBottom:'10px', marginTop:'20px',justifyContent:'space-between'}}>
                                <p style={{minWidth:'150px'}}>{moment(donnees?.eventDate).format('DD-MM-YYYY HH:mm')}</p>
                                <p>{donnees?.address}</p>
                            </div>
                    </div>
                    <div style={{backgroundColor:'var(--background-default-grey-hover)', display:'flex', alignItems:'center', flexDirection:'column', width:'90%', marginBottom:'40px'}}>
                        
                        <div style={{width:'100%', display:'flex', justifyContent:'center'}}><h3>Vous êtes intéréssé par cet évenement ?</h3></div>
                        {donnees !== undefined && <div style={{display:'flex', flexDirection:'column'}}>
                                    <div style={{fontSize:'3em', display:'flex', justifyContent:'center', textAlign:'center'}}>
                                        <Countdown date={delay} />
                                    </div>
                                    <div style={{display:'flex', alignItems:'center', justifyContent:'start', marginTop:'20px'}}>
                                        <div><p style={{marginLeft:'20px'}}>Jours</p></div>
                                        <div><p style={{marginLeft:'32px'}}>Heure</p></div>
                                        <div><p style={{marginLeft:'18px'}}>Minutes</p></div>
                                        <div><p style={{marginLeft:'8px'}}>Secondes</p></div>
                                    </div>
                                
                                </div>}
                        {windowWidth <= 700 && <div style={{display:'flex', alignItems:'center', flexDirection:'row', justifyContent:'space-evenly', width:'100%', height:'10vh'}}>
                        
                        <div style={{display:'flex', justifyContent:'space-around', alignItems:'center', flexDirection:'column', height:'5vh'}}>
                        <Button
                            iconId="fr-icon-checkbox-circle-line"
                            
                            onClick={function noRefCheck(){}}
                            priority="tertiary no outline"
                            title="Label button"
                            />
                            Partage
                            </div>
                            <div style={{display:'flex', justifyContent:'space-around', alignItems:'center', flexDirection:'column', height:'5vh'}}>
                           <Button
                                iconId="fr-icon-heart-fill"
                                aria-hidden="true"
                                onClick={function noRefCheck(){}}
                                priority="tertiary no outline"
                                title="Label button"
                                
                                />
                            
                            {donnees?.nbLike} Aime
                            </div>    
                            <div style={{display:'flex', justifyContent:'space-around', alignItems:'center', flexDirection:'column', height:'5vh'}}>
                            <Button
                                iconId="fr-icon-map-pin-user-fill"
                                aria-hidden="true"
                                onClick={function noRefCheck(){}}
                                priority="tertiary no outline"
                                title="Label button"
                            />
                            Je participe
                            </div>
                        </div>}
                        {windowWidth > 700 && <div style={{display:'flex', alignItems:'center', flexDirection:'row', justifyContent:'space-evenly', width:'100%', height:'10vh'}}>
                            <Button
                                iconId="fr-icon-checkbox-circle-line"
                                onClick={function noRefCheck(){}}
                                priority="secondary"
                                size="large"
                                style={{minWidth:'200px', display:'flex', justifyContent:'center'}}
                                >
                                Partager
                            </Button>
                            <Button
                                iconId="fr-icon-checkbox-circle-line"
                                onClick={function noRefCheck(){}}
                                priority="secondary"
                                size="large"
                                style={{minWidth:'200px', display:'flex', justifyContent:'center'}}
                                >
                                {donnees?.nbLike} Aimer
                            </Button>
                            <Button
                                iconId="fr-icon-checkbox-circle-line"
                                onClick={function noRefCheck(){}}
                                priority="secondary"
                                size="large"
                                style={{minWidth:'200px', display:'flex', justifyContent:'center'}}
                                >
                                Je participe !
                            </Button>
                        </div>}

                    </div>
                </div>
                <div style={{height:'400px', width:'100%', display:'flex', justifyContent:'center', alignItems:'center'}}>
               {donnees !== undefined && <MapContainer center={[donnees.latitude, donnees.longitude]} zoom={13} scrollWheelZoom={false} style={{height:'400px', width:'60%', display:'flex', justifyContent:'center', alignItems:'center', marginBottom:'40px'}}>
                    <TileLayer
                        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                    <Marker position={[donnees.latitude, donnees.longitude]}>
                        <Popup>
                        {donnees.address} <br />{moment(donnees.eventDate).format('DD-MM-YYYY HH:mm')}.
                        </Popup>
                    </Marker>
                </MapContainer>}
                </div>
                <FooterComp/>
            </div>
        
    );

    
}