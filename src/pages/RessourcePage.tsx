import { useState, useEffect} from 'react';
import NavBar from '../components/NavBar';
import RessourceCard  from "../components/RessourceCard";
import FooterComp from '../components/Footer';
import {Ressource} from "../model/Ressource";
import moment from 'moment';

const BASE_URL = 'http://localhost:8000/api';

function RessourcePage() {
    const [error, setError] = useState();
    const [isLoading, setIsLoading] = useState(false);
    const [donnees, setDonnees] = useState<Ressource[]>([]);

    useEffect(() => {
		const fetchData = async () => {
			setIsLoading(true);
            const currentDate = moment().format('YYYY-MM-DDTHH:mm:ss');
			try { 
				const response = await fetch(`${BASE_URL}/ressources?eventDate[after]=${currentDate}`);
                console.log(response);

				if (response.ok) {
					const responseData = await response.json();
                    const members = responseData['hydra:member'];
                    const result:Ressource[] =[]
                    members.forEach(member => {
                    if (moment(member.eventDate).isAfter(currentDate)){
                        result.push(member);
                    }
                });

            setDonnees(result);
					setIsLoading(false);
          
				} else {
				console.error('Erreur lors de la récupération des données');
				}

			} catch (e: any) {
				console.error('Erreur inattendue: ', e);
				setError(e);
			} finally {
				setIsLoading(false);
			}
		};
		fetchData();
	}, []);


    return (
        <div>  
            <NavBar/> 
            <h1>Ressources</h1>
            {isLoading && <div>Chargement en cours...</div>}
            {error && <div>Erreur: {error}</div>}
            <div style={{display:'flex', alignItems:'center', flexDirection:'column'}}>
                {donnees && donnees.map((ressource: Ressource) => (
                    <RessourceCard key={ressource.id} ressourceInfo={ressource}/>
                ))}
            </div>
            <FooterComp/>
        </div>
    );
}

export default RessourcePage;