import { NavLink } from 'react-router-dom';
import { MapContainer } from 'react-leaflet/MapContainer'
import { TileLayer } from 'react-leaflet/TileLayer'
import { Marker, Popup } from 'react-leaflet';
function HomePage() {
    return (
        <div>
            <nav>
                <ul>
                    <NavLink to="/">Home</NavLink>
                    <NavLink to="/ressource">Ressource</NavLink>
                </ul>
            </nav>
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '400px' }}>
            <MapContainer center={[41.100, -1]} zoom={13} scrollWheelZoom={false} style={{height:'400px', width:'60%', display:'flex', justifyContent:'center', alignItems:'center', marginBottom:'40px'}}>
                    <TileLayer
                        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                    <Marker position={[41.100, -1]}>
                        <Popup>
                            test
                        </Popup>
                    </Marker>
                </MapContainer>
            </div>
        </div>
    );
}

export default HomePage;