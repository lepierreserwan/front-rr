export interface Ressource {
    id: number;
    title: string;
    address: string;
    description: string;
    state: string;
    created_at: string;
    updated_at: string;
    eventDate: string;
    nbLike: number;
    longitude: number;
    latitude: number;
  }